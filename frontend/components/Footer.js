import { Container } from 'reactstrap';

const Footer = () => (
	<Container fluid className="bg-secondary px-0">
		<h3 className="text-center text-white py-3">&copy; 2019 Paddywack Homemade Gifts. All Rights Reserved.</h3>
	</Container>
);

export default Footer;
