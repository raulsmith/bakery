import React, { Component } from 'react';
import Header from '../components/Header';
import Footer from '../components/Footer';
import Meta from '../components/Meta';
import { Row, Col, Container } from 'reactstrap';

class Page extends Component {
	render() {
    	return (
    		<React.Fragment>
				<Meta />
				<Header />
				<Row>
					<Col>
						<Container fluid className="main-header">
							{this.props.children}
						</Container>
					</Col>
				</Row>
				<Footer />
			</React.Fragment>
    	);
	}
}

export default Page;
