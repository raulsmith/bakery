import React from 'react';
import { Collapse, Navbar, NavbarToggler, NavbarBrand, Nav, NavItem, NavLink } from 'reactstrap';

export default class Example extends React.Component {
	constructor(props) {
    	super(props);

    	this.toggleNavbar = this.toggleNavbar.bind(this);
    	this.state = {
    		isOpen: false
    	};
  	}

	toggleNavbar() {
    	this.setState({
    		isOpen: !this.state.isOpen
		});
	}
	render() {
		return (
			<div>
				<Navbar color="primary" dark expand="md">
					<NavbarBrand onClick={this.toggleNavbar}>
						<img src="http://galacticblue.net/img/logos/paddywack-v-2.png" height="44" alt="Paddywack Homemade Gifts" border="0" />
					</NavbarBrand>
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className="ml-auto" navbar>
							<NavItem className="px-4">
								<NavLink href="/">Home</NavLink>
							</NavItem>
							<NavItem className="px-4">
								<NavLink href="/about">About</NavLink>
							</NavItem>
							<NavItem className="px-4">
								<NavLink href="/gallery">Gallery</NavLink>
							</NavItem>
							<NavItem className="px-4">
								<NavLink href="/creator">Art Creator</NavLink>
							</NavItem>
							<NavItem className="px-4">
								<NavLink href="/shop">Shop</NavLink>
							</NavItem>
						</Nav>
					</Collapse>
				</Navbar>
			</div>
		);
	}
}