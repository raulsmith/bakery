import App, { Container } from 'next/app';
import Page from '../components/Page';
import withData from '../lib/withData';
import { PageTransition } from 'next-page-transitions';

class MyApp extends App {
	render() {
		
		const { Component } = this.props;
		
		return (
			<Container>
				<Page>
					<PageTransition timeout={300} classNames="page-transition">
						<Component />
					</PageTransition>
					<style jsx global>{`
						.page-transition-enter {
							opacity: 0;
						}
						.page-transition-enter-active {
							opacity: 1;
							transition: opacity 300ms;
						}
						.page-transition-exit {
							opacity: 1;
						}
						.page-transition-exit-active {
							opacity: 0;
							transition: opacity 300ms;
						}
					`}</style>
				</Page>
			</Container>
		);
	}
}

export default withData(MyApp);
